
//----------------------------------------------------
// The following code was generated by CUP v0.11b 20160615 (GIT 4ac7450)
//----------------------------------------------------

package Analizador;

/** CUP generated class containing symbol constants. */
public class sym {
  /* terminals */
  public static final int Else = 7;
  public static final int For = 10;
  public static final int Suma = 13;
  public static final int Numero = 32;
  public static final int Corchete_c = 27;
  public static final int Op_booleano = 21;
  public static final int ERROR = 33;
  public static final int Corchete_a = 26;
  public static final int Parent_c = 23;
  public static final int Identificador = 31;
  public static final int Parent_a = 22;
  public static final int Comillas = 3;
  public static final int Int = 11;
  public static final int Llave_c = 25;
  public static final int Llave_a = 24;
  public static final int Op_relacional = 18;
  public static final int P_coma = 29;
  public static final int T_dato = 4;
  public static final int Main = 28;
  public static final int Cadena = 5;
  public static final int EOF = 0;
  public static final int Division = 16;
  public static final int Op_incremento = 20;
  public static final int Op_atribucion = 19;
  public static final int Resta = 14;
  public static final int If = 6;
  public static final int Linea = 2;
  public static final int error = 1;
  public static final int Punto = 30;
  public static final int Op_logico = 17;
  public static final int Do = 8;
  public static final int Igual = 12;
  public static final int While = 9;
  public static final int Multiplicacion = 15;
  public static final String[] terminalNames = new String[] {
  "EOF",
  "error",
  "Linea",
  "Comillas",
  "T_dato",
  "Cadena",
  "If",
  "Else",
  "Do",
  "While",
  "For",
  "Int",
  "Igual",
  "Suma",
  "Resta",
  "Multiplicacion",
  "Division",
  "Op_logico",
  "Op_relacional",
  "Op_atribucion",
  "Op_incremento",
  "Op_booleano",
  "Parent_a",
  "Parent_c",
  "Llave_a",
  "Llave_c",
  "Corchete_a",
  "Corchete_c",
  "Main",
  "P_coma",
  "Punto",
  "Identificador",
  "Numero",
  "ERROR"
  };
}

