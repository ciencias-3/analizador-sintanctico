package Analizador;
import static Analizador.Tokens.*;
%%
%class Lexico
%type Tokens
L=[a-zA-Z_]+
D=[0-9]+
espacio=[ ,\t,\r]+
%{
    public String lexemas;
%}
%%
/* Espacios en blanco */
{espacio} {/*Ignore*/}

/* Comentarios */
( "//"(.)* ) {/*Ignore*/}

/* Salto de linea */
( "\n" ) {return Linea;}

/* Comillas */
( "\"" ) {lexemas=yytext(); return Comillas;}

/* Tipos de datos */
( int) {lexemas=yytext(); return T_dato_int;}
( char ) {lexemas=yytext(); return T_dato_char;}
( long) {lexemas=yytext(); return T_dato_long;}
( float ) {lexemas=yytext(); return T_dato_float;}
( double ) {lexemas=yytext(); return T_dato_double;}

/* Tipo de dato String */
( string ) {lexemas=yytext(); return Cadena;}

/* Palabra reservada If */
( if ) {lexemas=yytext(); return If;}


/* Palabras reservadas extras */

/* Auto*/
 ( "auto" ) {lexemas=yytext(); return Auto;}
 
/* break*/
 ( "break" ) {lexemas=yytext(); return Break;}
 
 /* case*/
 ( "case" ) {lexemas=yytext(); return Case;}

  /* const*/
 ( "const" ) {lexemas=yytext(); return Const;}
 
 /* continue*/
 ( "continue" ) {lexemas=yytext(); return Continue;}
 
  /* default*/
 ( "default" ) {lexemas=yytext(); return Default;}
 
  /* do*/
 ( "do" ) {lexemas=yytext(); return Do;}
 
 /* reservada else*/
 ( "else" ) {lexemas=yytext(); return Else;}
 
 /* enum*/
 ( "enum" ) {lexemas=yytext(); return Enum;}
 
 /* reservada extern*/
 ( "extern" ) {lexemas=yytext(); return Extern;}

/* reservada for*/
 ( "for" ) {lexemas=yytext(); return For;}

/* reservada goto*/
 ( "goto" ) {lexemas=yytext(); return Goto;}

/* reservada register*/
 ( "register" ) {lexemas=yytext(); return Register;}

/* reservada return*/
 ( "return" ) {lexemas=yytext(); return Return;}

/* reservada short*/
 ( "short" ) {lexemas=yytext(); return Short;}

/* reservada signed*/
 ( "signed" ) {lexemas=yytext(); return Signed;}

/* reservada sizeof*/
 ( "sizeof" ) {lexemas=yytext(); return Sizeof;}

/* reservada static*/
 ( "static" ) {lexemas=yytext(); return Static;}

/* reservada struct*/
 ( "struct" ) {lexemas=yytext(); return Struct;}

/* reservada switch*/
 ( "switch" ) {lexemas=yytext(); return Switch;}

/* reservada typedef*/
 ( "typedef" ) {lexemas=yytext(); return Typedef;}

/* reservada union*/
 ( "union" ) {lexemas=yytext(); return Union;}

/* reservada unsigned*/
 ( "unsigned" ) {lexemas=yytext(); return Unsigned;}

/* reservada void*/
 ( "void" ) {lexemas=yytext(); return Void;}

/* reservada volatile*/
 ( "volatile" ) {lexemas=yytext(); return Volatile;}

/* reservada while*/
 ( "while" ) {lexemas=yytext(); return While;}
 
 
 
/* Operador Igual */
( "=" ) {lexemas=yytext(); return Igual;}

/* Operador Suma */
( "+" ) {lexemas=yytext(); return Suma;}

/* Operador Resta */
( "-" ) {lexemas=yytext(); return Resta;}

/* Operador Multiplicacion */
( "*" ) {lexemas=yytext(); return Multiplicacion;}

/* Operador Division */
( "/" ) {lexemas=yytext(); return Division;}

/* Operadores logicos */
( "&&") {lexemas=yytext(); return Op_logico_and;}
( "||") {lexemas=yytext(); return Op_logico_or;}
( "!" ) {lexemas=yytext(); return Op_logico_negacion;}
( "&" ) {lexemas=yytext(); return Op_logico_ysolo;}
( "|" ) {lexemas=yytext(); return Op_logico_osolo;}

/*Operadores Relacionales */
( ">"  ) {lexemas = yytext(); return Op_relacional_mayor;}
( "<"  ) {lexemas = yytext(); return Op_relacional_menor;}
( "==" ) {lexemas = yytext(); return Op_relacional_igual;}
( "!=" ) {lexemas = yytext(); return Op_relacional_diferente;}
( ">=" ) {lexemas = yytext(); return Op_relacional_mayor_e_igual;}
( "<=" ) {lexemas = yytext(); return Op_relacional_menor_e_igual;}
( "<<" ) {lexemas = yytext(); return Op_relacional_salida_de_datos;}
( ">>" ) {lexemas = yytext(); return Op_relacional_entrada_de_datos;}

/* Operadores Atribucion */
( "+=" ) {lexemas = yytext(); return Op_atribucion_sumar_a_variable;}
( "-=" ) {lexemas = yytext(); return Op_atribucion_restar_a_variable;}
( "*=" ) {lexemas = yytext(); return Op_atribucion_multiplicar_a_variable;}
( "/=") {lexemas = yytext(); return Op_atribucion_dividir_a_variable;}
("%=" ) {lexemas = yytext(); return Op_atribucion_resto_a_variable;}

/* Operadores Incremento y decremento */
( "++") {lexemas = yytext(); return Op_incremento_aumentar;}
( "--" ) {lexemas = yytext(); return Op_incremento_disminuir;}

/*Operadores Booleanos*/
(true)      {lexemas = yytext(); return Op_booleano_true;}
(false)      {lexemas = yytext(); return Op_booleano_false;}

/* Parentesis de apertura */
( "(" ) {lexemas=yytext(); return Parentesis_a;}

/* Parentesis de cierre */
( ")" ) {lexemas=yytext(); return Parentesis_c;}

/* Llave de apertura */
( "{" ) {lexemas=yytext(); return Llave_a;}

/* Llave de cierre */
( "}" ) {lexemas=yytext(); return Llave_c;}

/* Corchete de apertura */
( "[" ) {lexemas = yytext(); return Corchete_a;}

/* Corchete de cierre */
( "]" ) {lexemas = yytext(); return Corchete_c;}

/* Marcador de inicio de algoritmo */
( "main" ) {lexemas=yytext(); return Main;}

/* Marcador de inicio de impresion en pantalla */
( "printf" ) {lexemas=yytext(); return Printf;}

/* Punto y coma */
( ";" ) {lexemas=yytext(); return P_coma;}

/* Punto */
( "." ) {lexemas=yytext(); return Punto;}


/* Identificador */
{L}({L}|{D})* {lexemas=yytext(); return Identificador;}

/* Numero */
("(-"{D}+")")|{D}+ {lexemas=yytext(); return Numero;}



/* Error de analisis */
 . {return ERROR;}

