// DO NOT EDIT
// Generated by JFlex 1.8.2 http://jflex.de/
// source: src/Analizador/Lexico.flex

package Analizador;
import static Analizador.Tokens.*;

// See https://github.com/jflex-de/jflex/issues/222
@SuppressWarnings("FallThrough")
class Lexico {

  /** This character denotes the end of file. */
  public static final int YYEOF = -1;

  /** Initial size of the lookahead buffer. */
  private static final int ZZ_BUFFERSIZE = 16384;

  // Lexical states.
  public static final int YYINITIAL = 0;

  /**
   * ZZ_LEXSTATE[l] is the state in the DFA for the lexical state l
   * ZZ_LEXSTATE[l+1] is the state in the DFA for the lexical state l
   *                  at the beginning of a line
   * l is of the form l = 2*k, k a non negative integer
   */
  private static final int ZZ_LEXSTATE[] = {
     0, 0
  };

  /**
   * Top-level table for translating characters to character classes
   */
  private static final int [] ZZ_CMAP_TOP = zzUnpackcmap_top();

  private static final String ZZ_CMAP_TOP_PACKED_0 =
    "\1\0\37\u0100\1\u0200\267\u0100\10\u0300\u1020\u0100";

  private static int [] zzUnpackcmap_top() {
    int [] result = new int[4352];
    int offset = 0;
    offset = zzUnpackcmap_top(ZZ_CMAP_TOP_PACKED_0, offset, result);
    return result;
  }

  private static int zzUnpackcmap_top(String packed, int offset, int [] result) {
    int i = 0;       /* index in packed string  */
    int j = offset;  /* index in unpacked array */
    int l = packed.length();
    while (i < l) {
      int count = packed.charAt(i++);
      int value = packed.charAt(i++);
      do result[j++] = value; while (--count > 0);
    }
    return j;
  }


  /**
   * Second-level tables for translating characters to character classes
   */
  private static final int [] ZZ_CMAP_BLOCKS = zzUnpackcmap_blocks();

  private static final String ZZ_CMAP_BLOCKS_PACKED_0 =
    "\11\0\1\1\1\2\2\3\1\4\22\0\1\1\1\5"+
    "\1\6\2\0\1\7\1\10\1\0\1\11\1\12\1\13"+
    "\1\14\1\1\1\15\1\16\1\17\12\20\1\0\1\21"+
    "\1\22\1\23\1\24\2\0\32\25\1\26\1\0\1\27"+
    "\1\0\1\25\1\0\1\30\1\31\1\32\1\33\1\34"+
    "\1\35\1\36\1\37\1\40\1\25\1\41\1\42\1\43"+
    "\1\44\1\45\1\46\1\25\1\47\1\50\1\51\1\52"+
    "\1\53\1\54\1\55\1\56\1\57\1\60\1\61\1\62"+
    "\7\0\1\3\u01a2\0\2\3\326\0\u0100\3";

  private static int [] zzUnpackcmap_blocks() {
    int [] result = new int[1024];
    int offset = 0;
    offset = zzUnpackcmap_blocks(ZZ_CMAP_BLOCKS_PACKED_0, offset, result);
    return result;
  }

  private static int zzUnpackcmap_blocks(String packed, int offset, int [] result) {
    int i = 0;       /* index in packed string  */
    int j = offset;  /* index in unpacked array */
    int l = packed.length();
    while (i < l) {
      int count = packed.charAt(i++);
      int value = packed.charAt(i++);
      do result[j++] = value; while (--count > 0);
    }
    return j;
  }

  /**
   * Translates DFA states to action switch labels.
   */
  private static final int [] ZZ_ACTION = zzUnpackAction();

  private static final String ZZ_ACTION_PACKED_0 =
    "\1\0\1\1\1\2\1\3\1\4\1\5\1\1\1\6"+
    "\1\7\1\10\1\11\1\12\1\13\1\14\1\15\1\16"+
    "\1\17\1\20\1\21\1\22\1\23\1\24\1\25\21\23"+
    "\1\26\1\27\1\30\1\31\1\32\1\33\1\0\1\34"+
    "\1\35\1\36\1\37\1\40\1\2\1\41\1\42\1\43"+
    "\1\44\1\45\1\46\6\23\1\47\7\23\1\50\16\23"+
    "\1\51\1\0\14\23\1\52\1\23\1\53\22\23\1\16"+
    "\1\54\1\23\1\55\1\56\4\23\1\57\1\60\3\23"+
    "\1\61\1\62\1\63\12\23\1\64\3\23\1\65\2\23"+
    "\1\66\1\67\4\23\1\70\1\71\3\23\1\72\7\23"+
    "\1\73\2\23\1\74\2\23\1\75\1\76\1\77\1\23"+
    "\1\100\1\101\1\102\1\103\1\104\1\105\1\106\4\23"+
    "\1\107\1\23\1\110\2\23\1\111\1\112\1\113\1\114";

  private static int [] zzUnpackAction() {
    int [] result = new int[206];
    int offset = 0;
    offset = zzUnpackAction(ZZ_ACTION_PACKED_0, offset, result);
    return result;
  }

  private static int zzUnpackAction(String packed, int offset, int [] result) {
    int i = 0;       /* index in packed string  */
    int j = offset;  /* index in unpacked array */
    int l = packed.length();
    while (i < l) {
      int count = packed.charAt(i++);
      int value = packed.charAt(i++);
      do result[j++] = value; while (--count > 0);
    }
    return j;
  }


  /**
   * Translates a state to a row index in the transition table
   */
  private static final int [] ZZ_ROWMAP = zzUnpackRowMap();

  private static final String ZZ_ROWMAP_PACKED_0 =
    "\0\0\0\63\0\146\0\63\0\231\0\63\0\314\0\377"+
    "\0\u0132\0\63\0\u0165\0\u0198\0\u01cb\0\63\0\u01fe\0\u0231"+
    "\0\63\0\u0264\0\u0297\0\u02ca\0\u02fd\0\63\0\63\0\u0330"+
    "\0\u0363\0\u0396\0\u03c9\0\u03fc\0\u042f\0\u0462\0\u0495\0\u04c8"+
    "\0\u04fb\0\u052e\0\u0561\0\u0594\0\u05c7\0\u05fa\0\u062d\0\u0660"+
    "\0\63\0\u0693\0\63\0\63\0\63\0\63\0\u06c6\0\63"+
    "\0\63\0\63\0\63\0\63\0\u06f9\0\63\0\63\0\63"+
    "\0\63\0\63\0\63\0\u072c\0\u075f\0\u0792\0\u07c5\0\u07f8"+
    "\0\u082b\0\u085e\0\u0891\0\u08c4\0\u08f7\0\u092a\0\u095d\0\u0990"+
    "\0\u09c3\0\u02fd\0\u09f6\0\u0a29\0\u0a5c\0\u0a8f\0\u0ac2\0\u0af5"+
    "\0\u0b28\0\u0b5b\0\u0b8e\0\u0bc1\0\u0bf4\0\u0c27\0\u0c5a\0\u0c8d"+
    "\0\63\0\u0cc0\0\u0cf3\0\u0d26\0\u0d59\0\u0d8c\0\u0dbf\0\u0df2"+
    "\0\u0e25\0\u0e58\0\u0e8b\0\u0ebe\0\u0ef1\0\u0f24\0\u02fd\0\u0f57"+
    "\0\u02fd\0\u0f8a\0\u0fbd\0\u0ff0\0\u1023\0\u1056\0\u1089\0\u10bc"+
    "\0\u10ef\0\u1122\0\u1155\0\u1188\0\u11bb\0\u11ee\0\u1221\0\u1254"+
    "\0\u1287\0\u12ba\0\u12ed\0\63\0\u02fd\0\u1320\0\u02fd\0\u02fd"+
    "\0\u1353\0\u1386\0\u13b9\0\u13ec\0\u02fd\0\u02fd\0\u141f\0\u1452"+
    "\0\u1485\0\u02fd\0\u02fd\0\u02fd\0\u14b8\0\u14eb\0\u151e\0\u1551"+
    "\0\u1584\0\u15b7\0\u15ea\0\u161d\0\u1650\0\u1683\0\u02fd\0\u16b6"+
    "\0\u16e9\0\u171c\0\u02fd\0\u174f\0\u1782\0\u02fd\0\u02fd\0\u17b5"+
    "\0\u17e8\0\u181b\0\u184e\0\u02fd\0\u02fd\0\u1881\0\u18b4\0\u18e7"+
    "\0\u02fd\0\u191a\0\u194d\0\u1980\0\u19b3\0\u19e6\0\u1a19\0\u1a4c"+
    "\0\u02fd\0\u1a7f\0\u1ab2\0\u02fd\0\u1ae5\0\u1b18\0\u02fd\0\u02fd"+
    "\0\u02fd\0\u1b4b\0\u02fd\0\u02fd\0\u02fd\0\u02fd\0\u02fd\0\u02fd"+
    "\0\u02fd\0\u1b7e\0\u1bb1\0\u1be4\0\u1c17\0\u02fd\0\u1c4a\0\u02fd"+
    "\0\u1c7d\0\u1cb0\0\u02fd\0\u02fd\0\u02fd\0\u02fd";

  private static int [] zzUnpackRowMap() {
    int [] result = new int[206];
    int offset = 0;
    offset = zzUnpackRowMap(ZZ_ROWMAP_PACKED_0, offset, result);
    return result;
  }

  private static int zzUnpackRowMap(String packed, int offset, int [] result) {
    int i = 0;  /* index in packed string  */
    int j = offset;  /* index in unpacked array */
    int l = packed.length();
    while (i < l) {
      int high = packed.charAt(i++) << 16;
      result[j++] = high | packed.charAt(i++);
    }
    return j;
  }

  /**
   * The transition table of the DFA
   */
  private static final int [] ZZ_TRANS = zzUnpackTrans();

  private static final String ZZ_TRANS_PACKED_0 =
    "\1\2\1\3\1\4\1\0\1\3\1\5\1\6\1\7"+
    "\1\10\1\11\1\12\1\13\1\14\1\15\1\16\1\17"+
    "\1\20\1\21\1\22\1\23\1\24\1\25\1\26\1\27"+
    "\1\30\1\31\1\32\1\33\1\34\1\35\1\36\1\25"+
    "\1\37\1\25\1\40\1\41\2\25\1\42\1\43\1\44"+
    "\1\45\1\46\1\47\1\50\3\25\1\51\1\52\1\53"+
    "\64\0\1\3\2\0\1\3\101\0\1\54\62\0\1\55"+
    "\47\0\1\56\67\0\1\57\70\0\1\60\53\0\1\61"+
    "\6\0\1\62\54\0\1\63\5\0\1\64\56\0\1\65"+
    "\3\0\1\66\57\0\1\20\64\0\1\67\1\70\62\0"+
    "\1\71\62\0\1\72\1\73\56\0\1\25\4\0\1\25"+
    "\2\0\30\25\23\0\1\25\4\0\1\25\2\0\22\25"+
    "\1\74\5\25\23\0\1\25\4\0\1\25\2\0\17\25"+
    "\1\75\10\25\23\0\1\25\4\0\1\25\2\0\1\76"+
    "\6\25\1\77\5\25\1\100\12\25\23\0\1\25\4\0"+
    "\1\25\2\0\4\25\1\101\10\25\1\102\12\25\23\0"+
    "\1\25\4\0\1\25\2\0\12\25\1\103\1\25\1\104"+
    "\10\25\1\105\2\25\23\0\1\25\4\0\1\25\2\0"+
    "\1\106\11\25\1\107\2\25\1\110\12\25\23\0\1\25"+
    "\4\0\1\25\2\0\15\25\1\111\12\25\23\0\1\25"+
    "\4\0\1\25\2\0\5\25\1\112\6\25\1\113\13\25"+
    "\23\0\1\25\4\0\1\25\2\0\15\25\1\114\12\25"+
    "\23\0\1\25\4\0\1\25\2\0\1\115\27\25\23\0"+
    "\1\25\4\0\1\25\2\0\17\25\1\116\10\25\23\0"+
    "\1\25\4\0\1\25\2\0\4\25\1\117\23\25\23\0"+
    "\1\25\4\0\1\25\2\0\7\25\1\120\1\121\10\25"+
    "\1\122\2\25\1\123\3\25\23\0\1\25\4\0\1\25"+
    "\2\0\17\25\1\124\6\25\1\125\1\25\23\0\1\25"+
    "\4\0\1\25\2\0\14\25\1\126\13\25\23\0\1\25"+
    "\4\0\1\25\2\0\15\25\1\127\12\25\23\0\1\25"+
    "\4\0\1\25\2\0\7\25\1\130\20\25\64\0\1\131"+
    "\21\0\1\132\42\0\2\65\3\0\56\65\20\0\1\25"+
    "\4\0\1\25\2\0\21\25\1\133\6\25\23\0\1\25"+
    "\4\0\1\25\2\0\4\25\1\134\23\25\23\0\1\25"+
    "\4\0\1\25\2\0\20\25\1\135\7\25\23\0\1\25"+
    "\4\0\1\25\2\0\1\136\27\25\23\0\1\25\4\0"+
    "\1\25\2\0\14\25\1\137\13\25\23\0\1\25\4\0"+
    "\1\25\2\0\5\25\1\140\22\25\23\0\1\25\4\0"+
    "\1\25\2\0\22\25\1\141\5\25\23\0\1\25\4\0"+
    "\1\25\2\0\20\25\1\142\7\25\23\0\1\25\4\0"+
    "\1\25\2\0\22\25\1\143\5\25\23\0\1\25\4\0"+
    "\1\25\2\0\21\25\1\144\6\25\23\0\1\25\4\0"+
    "\1\25\2\0\12\25\1\145\15\25\23\0\1\25\4\0"+
    "\1\25\2\0\15\25\1\146\12\25\23\0\1\25\4\0"+
    "\1\25\2\0\17\25\1\147\10\25\23\0\1\25\4\0"+
    "\1\25\2\0\21\25\1\150\6\25\23\0\1\25\4\0"+
    "\1\25\2\0\21\25\1\151\6\25\23\0\1\25\4\0"+
    "\1\25\2\0\14\25\1\152\13\25\23\0\1\25\4\0"+
    "\1\25\2\0\10\25\1\153\17\25\23\0\1\25\4\0"+
    "\1\25\2\0\10\25\1\154\17\25\23\0\1\25\4\0"+
    "\1\25\2\0\6\25\1\155\12\25\1\156\6\25\23\0"+
    "\1\25\4\0\1\25\2\0\15\25\1\157\12\25\23\0"+
    "\1\25\4\0\1\25\2\0\6\25\1\160\20\25\1\161"+
    "\23\0\1\25\4\0\1\25\2\0\1\162\16\25\1\163"+
    "\10\25\23\0\1\25\4\0\1\25\2\0\10\25\1\164"+
    "\17\25\23\0\1\25\4\0\1\25\2\0\22\25\1\165"+
    "\5\25\23\0\1\25\4\0\1\25\2\0\16\25\1\166"+
    "\11\25\23\0\1\25\4\0\1\25\2\0\10\25\1\167"+
    "\7\25\1\170\7\25\23\0\1\25\4\0\1\25\2\0"+
    "\10\25\1\171\1\25\1\172\15\25\23\0\1\25\4\0"+
    "\1\25\2\0\10\25\1\173\17\25\15\0\1\174\5\0"+
    "\1\132\62\0\1\25\4\0\1\25\2\0\15\25\1\175"+
    "\12\25\23\0\1\25\4\0\1\25\2\0\1\176\27\25"+
    "\23\0\1\25\4\0\1\25\2\0\4\25\1\177\23\25"+
    "\23\0\1\25\4\0\1\25\2\0\17\25\1\200\10\25"+
    "\23\0\1\25\4\0\1\25\2\0\20\25\1\201\1\202"+
    "\6\25\23\0\1\25\4\0\1\25\2\0\1\203\27\25"+
    "\23\0\1\25\4\0\1\25\2\0\1\25\1\204\26\25"+
    "\23\0\1\25\4\0\1\25\2\0\4\25\1\205\23\25"+
    "\23\0\1\25\4\0\1\25\2\0\13\25\1\206\14\25"+
    "\23\0\1\25\4\0\1\25\2\0\4\25\1\207\23\25"+
    "\23\0\1\25\4\0\1\25\2\0\20\25\1\210\7\25"+
    "\23\0\1\25\4\0\1\25\2\0\1\211\27\25\23\0"+
    "\1\25\4\0\1\25\2\0\15\25\1\212\12\25\23\0"+
    "\1\25\4\0\1\25\2\0\6\25\1\213\21\25\23\0"+
    "\1\25\4\0\1\25\2\0\14\25\1\214\13\25\23\0"+
    "\1\25\4\0\1\25\2\0\14\25\1\215\13\25\23\0"+
    "\1\25\4\0\1\25\2\0\10\25\1\216\17\25\23\0"+
    "\1\25\4\0\1\25\2\0\22\25\1\217\5\25\23\0"+
    "\1\25\4\0\1\25\2\0\17\25\1\220\10\25\23\0"+
    "\1\25\4\0\1\25\2\0\14\25\1\221\13\25\23\0"+
    "\1\25\4\0\1\25\2\0\4\25\1\222\23\25\23\0"+
    "\1\25\4\0\1\25\2\0\21\25\1\223\6\25\23\0"+
    "\1\25\4\0\1\25\2\0\10\25\1\224\11\25\1\225"+
    "\5\25\23\0\1\25\4\0\1\25\2\0\21\25\1\226"+
    "\6\25\23\0\1\25\4\0\1\25\2\0\4\25\1\227"+
    "\23\25\23\0\1\25\4\0\1\25\2\0\4\25\1\230"+
    "\23\25\23\0\1\25\4\0\1\25\2\0\15\25\1\231"+
    "\12\25\23\0\1\25\4\0\1\25\2\0\10\25\1\232"+
    "\17\25\23\0\1\25\4\0\1\25\2\0\3\25\1\233"+
    "\24\25\23\0\1\25\4\0\1\25\2\0\1\234\27\25"+
    "\23\0\1\25\4\0\1\25\2\0\12\25\1\235\15\25"+
    "\23\0\1\25\4\0\1\25\2\0\11\25\1\236\16\25"+
    "\23\0\1\25\4\0\1\25\2\0\21\25\1\237\6\25"+
    "\23\0\1\25\4\0\1\25\2\0\10\25\1\240\17\25"+
    "\23\0\1\25\4\0\1\25\2\0\22\25\1\241\5\25"+
    "\23\0\1\25\4\0\1\25\2\0\12\25\1\242\15\25"+
    "\23\0\1\25\4\0\1\25\2\0\17\25\1\243\10\25"+
    "\23\0\1\25\4\0\1\25\2\0\4\25\1\244\23\25"+
    "\23\0\1\25\4\0\1\25\2\0\21\25\1\245\6\25"+
    "\23\0\1\25\4\0\1\25\2\0\21\25\1\246\6\25"+
    "\23\0\1\25\4\0\1\25\2\0\20\25\1\247\7\25"+
    "\23\0\1\25\4\0\1\25\2\0\17\25\1\250\10\25"+
    "\23\0\1\25\4\0\1\25\2\0\21\25\1\251\6\25"+
    "\23\0\1\25\4\0\1\25\2\0\4\25\1\252\23\25"+
    "\23\0\1\25\4\0\1\25\2\0\15\25\1\253\12\25"+
    "\23\0\1\25\4\0\1\25\2\0\10\25\1\254\17\25"+
    "\23\0\1\25\4\0\1\25\2\0\14\25\1\255\13\25"+
    "\23\0\1\25\4\0\1\25\2\0\2\25\1\256\25\25"+
    "\23\0\1\25\4\0\1\25\2\0\2\25\1\257\25\25"+
    "\23\0\1\25\4\0\1\25\2\0\3\25\1\260\24\25"+
    "\23\0\1\25\4\0\1\25\2\0\14\25\1\261\13\25"+
    "\23\0\1\25\4\0\1\25\2\0\6\25\1\262\21\25"+
    "\23\0\1\25\4\0\1\25\2\0\21\25\1\263\6\25"+
    "\23\0\1\25\4\0\1\25\2\0\4\25\1\264\23\25"+
    "\23\0\1\25\4\0\1\25\2\0\14\25\1\265\13\25"+
    "\23\0\1\25\4\0\1\25\2\0\12\25\1\266\15\25"+
    "\23\0\1\25\4\0\1\25\2\0\4\25\1\267\23\25"+
    "\23\0\1\25\4\0\1\25\2\0\14\25\1\270\13\25"+
    "\23\0\1\25\4\0\1\25\2\0\5\25\1\271\22\25"+
    "\23\0\1\25\4\0\1\25\2\0\21\25\1\272\6\25"+
    "\23\0\1\25\4\0\1\25\2\0\14\25\1\273\13\25"+
    "\23\0\1\25\4\0\1\25\2\0\3\25\1\274\24\25"+
    "\23\0\1\25\4\0\1\25\2\0\5\25\1\275\22\25"+
    "\23\0\1\25\4\0\1\25\2\0\2\25\1\276\25\25"+
    "\23\0\1\25\4\0\1\25\2\0\6\25\1\277\21\25"+
    "\23\0\1\25\4\0\1\25\2\0\21\25\1\300\6\25"+
    "\23\0\1\25\4\0\1\25\2\0\7\25\1\301\20\25"+
    "\23\0\1\25\4\0\1\25\2\0\4\25\1\302\23\25"+
    "\23\0\1\25\4\0\1\25\2\0\14\25\1\303\13\25"+
    "\23\0\1\25\4\0\1\25\2\0\10\25\1\304\17\25"+
    "\23\0\1\25\4\0\1\25\2\0\22\25\1\305\5\25"+
    "\23\0\1\25\4\0\1\25\2\0\21\25\1\306\6\25"+
    "\23\0\1\25\4\0\1\25\2\0\4\25\1\307\23\25"+
    "\23\0\1\25\4\0\1\25\2\0\5\25\1\310\22\25"+
    "\23\0\1\25\4\0\1\25\2\0\4\25\1\311\23\25"+
    "\23\0\1\25\4\0\1\25\2\0\12\25\1\312\15\25"+
    "\23\0\1\25\4\0\1\25\2\0\4\25\1\313\23\25"+
    "\23\0\1\25\4\0\1\25\2\0\17\25\1\314\10\25"+
    "\23\0\1\25\4\0\1\25\2\0\3\25\1\315\24\25"+
    "\23\0\1\25\4\0\1\25\2\0\4\25\1\316\23\25"+
    "\3\0";

  private static int [] zzUnpackTrans() {
    int [] result = new int[7395];
    int offset = 0;
    offset = zzUnpackTrans(ZZ_TRANS_PACKED_0, offset, result);
    return result;
  }

  private static int zzUnpackTrans(String packed, int offset, int [] result) {
    int i = 0;       /* index in packed string  */
    int j = offset;  /* index in unpacked array */
    int l = packed.length();
    while (i < l) {
      int count = packed.charAt(i++);
      int value = packed.charAt(i++);
      value--;
      do result[j++] = value; while (--count > 0);
    }
    return j;
  }


  /** Error code for "Unknown internal scanner error". */
  private static final int ZZ_UNKNOWN_ERROR = 0;
  /** Error code for "could not match input". */
  private static final int ZZ_NO_MATCH = 1;
  /** Error code for "pushback value was too large". */
  private static final int ZZ_PUSHBACK_2BIG = 2;

  /**
   * Error messages for {@link #ZZ_UNKNOWN_ERROR}, {@link #ZZ_NO_MATCH}, and
   * {@link #ZZ_PUSHBACK_2BIG} respectively.
   */
  private static final String ZZ_ERROR_MSG[] = {
    "Unknown internal scanner error",
    "Error: could not match input",
    "Error: pushback value was too large"
  };

  /**
   * ZZ_ATTRIBUTE[aState] contains the attributes of state {@code aState}
   */
  private static final int [] ZZ_ATTRIBUTE = zzUnpackAttribute();

  private static final String ZZ_ATTRIBUTE_PACKED_0 =
    "\1\0\1\11\1\1\1\11\1\1\1\11\3\1\1\11"+
    "\3\1\1\11\2\1\1\11\4\1\2\11\21\1\1\11"+
    "\1\1\4\11\1\0\5\11\1\1\6\11\35\1\1\11"+
    "\1\0\41\1\1\11\122\1";

  private static int [] zzUnpackAttribute() {
    int [] result = new int[206];
    int offset = 0;
    offset = zzUnpackAttribute(ZZ_ATTRIBUTE_PACKED_0, offset, result);
    return result;
  }

  private static int zzUnpackAttribute(String packed, int offset, int [] result) {
    int i = 0;       /* index in packed string  */
    int j = offset;  /* index in unpacked array */
    int l = packed.length();
    while (i < l) {
      int count = packed.charAt(i++);
      int value = packed.charAt(i++);
      do result[j++] = value; while (--count > 0);
    }
    return j;
  }

  /** Input device. */
  private java.io.Reader zzReader;

  /** Current state of the DFA. */
  private int zzState;

  /** Current lexical state. */
  private int zzLexicalState = YYINITIAL;

  /**
   * This buffer contains the current text to be matched and is the source of the {@link #yytext()}
   * string.
   */
  private char zzBuffer[] = new char[ZZ_BUFFERSIZE];

  /** Text position at the last accepting state. */
  private int zzMarkedPos;

  /** Current text position in the buffer. */
  private int zzCurrentPos;

  /** Marks the beginning of the {@link #yytext()} string in the buffer. */
  private int zzStartRead;

  /** Marks the last character in the buffer, that has been read from input. */
  private int zzEndRead;

  /**
   * Whether the scanner is at the end of file.
   * @see #yyatEOF
   */
  private boolean zzAtEOF;

  /**
   * The number of occupied positions in {@link #zzBuffer} beyond {@link #zzEndRead}.
   *
   * <p>When a lead/high surrogate has been read from the input stream into the final
   * {@link #zzBuffer} position, this will have a value of 1; otherwise, it will have a value of 0.
   */
  private int zzFinalHighSurrogate = 0;

  /** Number of newlines encountered up to the start of the matched text. */
  @SuppressWarnings("unused")
  private int yyline;

  /** Number of characters from the last newline up to the start of the matched text. */
  @SuppressWarnings("unused")
  private int yycolumn;

  /** Number of characters up to the start of the matched text. */
  @SuppressWarnings("unused")
  private long yychar;

  /** Whether the scanner is currently at the beginning of a line. */
  @SuppressWarnings("unused")
  private boolean zzAtBOL = true;

  /** Whether the user-EOF-code has already been executed. */
  @SuppressWarnings("unused")
  private boolean zzEOFDone;

  /* user code: */
    public String lexemas;


  /**
   * Creates a new scanner
   *
   * @param   in  the java.io.Reader to read input from.
   */
  Lexico(java.io.Reader in) {
    this.zzReader = in;
  }

  /**
   * Translates raw input code points to DFA table row
   */
  private static int zzCMap(int input) {
    int offset = input & 255;
    return offset == input ? ZZ_CMAP_BLOCKS[offset] : ZZ_CMAP_BLOCKS[ZZ_CMAP_TOP[input >> 8] | offset];
  }

  /**
   * Refills the input buffer.
   *
   * @return {@code false} iff there was new input.
   * @exception java.io.IOException  if any I/O-Error occurs
   */
  private boolean zzRefill() throws java.io.IOException {

    /* first: make room (if you can) */
    if (zzStartRead > 0) {
      zzEndRead += zzFinalHighSurrogate;
      zzFinalHighSurrogate = 0;
      System.arraycopy(zzBuffer, zzStartRead,
                       zzBuffer, 0,
                       zzEndRead - zzStartRead);

      /* translate stored positions */
      zzEndRead -= zzStartRead;
      zzCurrentPos -= zzStartRead;
      zzMarkedPos -= zzStartRead;
      zzStartRead = 0;
    }

    /* is the buffer big enough? */
    if (zzCurrentPos >= zzBuffer.length - zzFinalHighSurrogate) {
      /* if not: blow it up */
      char newBuffer[] = new char[zzBuffer.length * 2];
      System.arraycopy(zzBuffer, 0, newBuffer, 0, zzBuffer.length);
      zzBuffer = newBuffer;
      zzEndRead += zzFinalHighSurrogate;
      zzFinalHighSurrogate = 0;
    }

    /* fill the buffer with new input */
    int requested = zzBuffer.length - zzEndRead;
    int numRead = zzReader.read(zzBuffer, zzEndRead, requested);

    /* not supposed to occur according to specification of java.io.Reader */
    if (numRead == 0) {
      throw new java.io.IOException(
          "Reader returned 0 characters. See JFlex examples/zero-reader for a workaround.");
    }
    if (numRead > 0) {
      zzEndRead += numRead;
      if (Character.isHighSurrogate(zzBuffer[zzEndRead - 1])) {
        if (numRead == requested) { // We requested too few chars to encode a full Unicode character
          --zzEndRead;
          zzFinalHighSurrogate = 1;
        } else {                    // There is room in the buffer for at least one more char
          int c = zzReader.read();  // Expecting to read a paired low surrogate char
          if (c == -1) {
            return true;
          } else {
            zzBuffer[zzEndRead++] = (char)c;
          }
        }
      }
      /* potentially more input available */
      return false;
    }

    /* numRead < 0 ==> end of stream */
    return true;
  }


  /**
   * Closes the input reader.
   *
   * @throws java.io.IOException if the reader could not be closed.
   */
  public final void yyclose() throws java.io.IOException {
    zzAtEOF = true; // indicate end of file
    zzEndRead = zzStartRead; // invalidate buffer

    if (zzReader != null) {
      zzReader.close();
    }
  }


  /**
   * Resets the scanner to read from a new input stream.
   *
   * <p>Does not close the old reader.
   *
   * <p>All internal variables are reset, the old input stream <b>cannot</b> be reused (internal
   * buffer is discarded and lost). Lexical state is set to {@code ZZ_INITIAL}.
   *
   * <p>Internal scan buffer is resized down to its initial length, if it has grown.
   *
   * @param reader The new input stream.
   */
  public final void yyreset(java.io.Reader reader) {
    zzReader = reader;
    zzEOFDone = false;
    yyResetPosition();
    zzLexicalState = YYINITIAL;
    if (zzBuffer.length > ZZ_BUFFERSIZE) {
      zzBuffer = new char[ZZ_BUFFERSIZE];
    }
  }

  /**
   * Resets the input position.
   */
  private final void yyResetPosition() {
      zzAtBOL  = true;
      zzAtEOF  = false;
      zzCurrentPos = 0;
      zzMarkedPos = 0;
      zzStartRead = 0;
      zzEndRead = 0;
      zzFinalHighSurrogate = 0;
      yyline = 0;
      yycolumn = 0;
      yychar = 0L;
  }


  /**
   * Returns whether the scanner has reached the end of the reader it reads from.
   *
   * @return whether the scanner has reached EOF.
   */
  public final boolean yyatEOF() {
    return zzAtEOF;
  }


  /**
   * Returns the current lexical state.
   *
   * @return the current lexical state.
   */
  public final int yystate() {
    return zzLexicalState;
  }


  /**
   * Enters a new lexical state.
   *
   * @param newState the new lexical state
   */
  public final void yybegin(int newState) {
    zzLexicalState = newState;
  }


  /**
   * Returns the text matched by the current regular expression.
   *
   * @return the matched text.
   */
  public final String yytext() {
    return new String(zzBuffer, zzStartRead, zzMarkedPos-zzStartRead);
  }


  /**
   * Returns the character at the given position from the matched text.
   *
   * <p>It is equivalent to {@code yytext().charAt(pos)}, but faster.
   *
   * @param position the position of the character to fetch. A value from 0 to {@code yylength()-1}.
   *
   * @return the character at {@code position}.
   */
  public final char yycharat(int position) {
    return zzBuffer[zzStartRead + position];
  }


  /**
   * How many characters were matched.
   *
   * @return the length of the matched text region.
   */
  public final int yylength() {
    return zzMarkedPos-zzStartRead;
  }


  /**
   * Reports an error that occurred while scanning.
   *
   * <p>In a well-formed scanner (no or only correct usage of {@code yypushback(int)} and a
   * match-all fallback rule) this method will only be called with things that
   * "Can't Possibly Happen".
   *
   * <p>If this method is called, something is seriously wrong (e.g. a JFlex bug producing a faulty
   * scanner etc.).
   *
   * <p>Usual syntax/scanner level error handling should be done in error fallback rules.
   *
   * @param errorCode the code of the error message to display.
   */
  private static void zzScanError(int errorCode) {
    String message;
    try {
      message = ZZ_ERROR_MSG[errorCode];
    } catch (ArrayIndexOutOfBoundsException e) {
      message = ZZ_ERROR_MSG[ZZ_UNKNOWN_ERROR];
    }

    throw new Error(message);
  }


  /**
   * Pushes the specified amount of characters back into the input stream.
   *
   * <p>They will be read again by then next call of the scanning method.
   *
   * @param number the number of characters to be read again. This number must not be greater than
   *     {@link #yylength()}.
   */
  public void yypushback(int number)  {
    if ( number > yylength() )
      zzScanError(ZZ_PUSHBACK_2BIG);

    zzMarkedPos -= number;
  }




  /**
   * Resumes scanning until the next regular expression is matched, the end of input is encountered
   * or an I/O-Error occurs.
   *
   * @return the next token.
   * @exception java.io.IOException if any I/O-Error occurs.
   */
  public Tokens yylex() throws java.io.IOException {
    int zzInput;
    int zzAction;

    // cached fields:
    int zzCurrentPosL;
    int zzMarkedPosL;
    int zzEndReadL = zzEndRead;
    char[] zzBufferL = zzBuffer;

    int [] zzTransL = ZZ_TRANS;
    int [] zzRowMapL = ZZ_ROWMAP;
    int [] zzAttrL = ZZ_ATTRIBUTE;

    while (true) {
      zzMarkedPosL = zzMarkedPos;

      zzAction = -1;

      zzCurrentPosL = zzCurrentPos = zzStartRead = zzMarkedPosL;

      zzState = ZZ_LEXSTATE[zzLexicalState];

      // set up zzAction for empty match case:
      int zzAttributes = zzAttrL[zzState];
      if ( (zzAttributes & 1) == 1 ) {
        zzAction = zzState;
      }


      zzForAction: {
        while (true) {

          if (zzCurrentPosL < zzEndReadL) {
            zzInput = Character.codePointAt(zzBufferL, zzCurrentPosL, zzEndReadL);
            zzCurrentPosL += Character.charCount(zzInput);
          }
          else if (zzAtEOF) {
            zzInput = YYEOF;
            break zzForAction;
          }
          else {
            // store back cached positions
            zzCurrentPos  = zzCurrentPosL;
            zzMarkedPos   = zzMarkedPosL;
            boolean eof = zzRefill();
            // get translated positions and possibly new buffer
            zzCurrentPosL  = zzCurrentPos;
            zzMarkedPosL   = zzMarkedPos;
            zzBufferL      = zzBuffer;
            zzEndReadL     = zzEndRead;
            if (eof) {
              zzInput = YYEOF;
              break zzForAction;
            }
            else {
              zzInput = Character.codePointAt(zzBufferL, zzCurrentPosL, zzEndReadL);
              zzCurrentPosL += Character.charCount(zzInput);
            }
          }
          int zzNext = zzTransL[ zzRowMapL[zzState] + zzCMap(zzInput) ];
          if (zzNext == -1) break zzForAction;
          zzState = zzNext;

          zzAttributes = zzAttrL[zzState];
          if ( (zzAttributes & 1) == 1 ) {
            zzAction = zzState;
            zzMarkedPosL = zzCurrentPosL;
            if ( (zzAttributes & 8) == 8 ) break zzForAction;
          }

        }
      }

      // store back cached position
      zzMarkedPos = zzMarkedPosL;

      if (zzInput == YYEOF && zzStartRead == zzCurrentPos) {
        zzAtEOF = true;
        return null;
      }
      else {
        switch (zzAction < 0 ? zzAction : ZZ_ACTION[zzAction]) {
          case 1:
            { return ERROR;
            }
            // fall through
          case 77: break;
          case 2:
            { /*Ignore*/
            }
            // fall through
          case 78: break;
          case 3:
            { return Linea;
            }
            // fall through
          case 79: break;
          case 4:
            { lexemas=yytext(); return Op_logico_negacion;
            }
            // fall through
          case 80: break;
          case 5:
            { lexemas=yytext(); return Comillas;
            }
            // fall through
          case 81: break;
          case 6:
            { lexemas=yytext(); return Op_logico_ysolo;
            }
            // fall through
          case 82: break;
          case 7:
            { lexemas=yytext(); return Parentesis_a;
            }
            // fall through
          case 83: break;
          case 8:
            { lexemas=yytext(); return Parentesis_c;
            }
            // fall through
          case 84: break;
          case 9:
            { lexemas=yytext(); return Multiplicacion;
            }
            // fall through
          case 85: break;
          case 10:
            { lexemas=yytext(); return Suma;
            }
            // fall through
          case 86: break;
          case 11:
            { lexemas=yytext(); return Resta;
            }
            // fall through
          case 87: break;
          case 12:
            { lexemas=yytext(); return Punto;
            }
            // fall through
          case 88: break;
          case 13:
            { lexemas=yytext(); return Division;
            }
            // fall through
          case 89: break;
          case 14:
            { lexemas=yytext(); return Numero;
            }
            // fall through
          case 90: break;
          case 15:
            { lexemas=yytext(); return P_coma;
            }
            // fall through
          case 91: break;
          case 16:
            { lexemas = yytext(); return Op_relacional_menor;
            }
            // fall through
          case 92: break;
          case 17:
            { lexemas=yytext(); return Igual;
            }
            // fall through
          case 93: break;
          case 18:
            { lexemas = yytext(); return Op_relacional_mayor;
            }
            // fall through
          case 94: break;
          case 19:
            { lexemas=yytext(); return Identificador;
            }
            // fall through
          case 95: break;
          case 20:
            { lexemas = yytext(); return Corchete_a;
            }
            // fall through
          case 96: break;
          case 21:
            { lexemas = yytext(); return Corchete_c;
            }
            // fall through
          case 97: break;
          case 22:
            { lexemas=yytext(); return Llave_a;
            }
            // fall through
          case 98: break;
          case 23:
            { lexemas=yytext(); return Op_logico_osolo;
            }
            // fall through
          case 99: break;
          case 24:
            { lexemas=yytext(); return Llave_c;
            }
            // fall through
          case 100: break;
          case 25:
            { lexemas = yytext(); return Op_relacional_diferente;
            }
            // fall through
          case 101: break;
          case 26:
            { lexemas = yytext(); return Op_atribucion_resto_a_variable;
            }
            // fall through
          case 102: break;
          case 27:
            { lexemas=yytext(); return Op_logico_and;
            }
            // fall through
          case 103: break;
          case 28:
            { lexemas = yytext(); return Op_atribucion_multiplicar_a_variable;
            }
            // fall through
          case 104: break;
          case 29:
            { lexemas = yytext(); return Op_incremento_aumentar;
            }
            // fall through
          case 105: break;
          case 30:
            { lexemas = yytext(); return Op_atribucion_sumar_a_variable;
            }
            // fall through
          case 106: break;
          case 31:
            { lexemas = yytext(); return Op_incremento_disminuir;
            }
            // fall through
          case 107: break;
          case 32:
            { lexemas = yytext(); return Op_atribucion_restar_a_variable;
            }
            // fall through
          case 108: break;
          case 33:
            { lexemas = yytext(); return Op_atribucion_dividir_a_variable;
            }
            // fall through
          case 109: break;
          case 34:
            { lexemas = yytext(); return Op_relacional_salida_de_datos;
            }
            // fall through
          case 110: break;
          case 35:
            { lexemas = yytext(); return Op_relacional_menor_e_igual;
            }
            // fall through
          case 111: break;
          case 36:
            { lexemas = yytext(); return Op_relacional_igual;
            }
            // fall through
          case 112: break;
          case 37:
            { lexemas = yytext(); return Op_relacional_mayor_e_igual;
            }
            // fall through
          case 113: break;
          case 38:
            { lexemas = yytext(); return Op_relacional_entrada_de_datos;
            }
            // fall through
          case 114: break;
          case 39:
            { lexemas=yytext(); return Do;
            }
            // fall through
          case 115: break;
          case 40:
            { lexemas=yytext(); return If;
            }
            // fall through
          case 116: break;
          case 41:
            { lexemas=yytext(); return Op_logico_or;
            }
            // fall through
          case 117: break;
          case 42:
            { lexemas=yytext(); return For;
            }
            // fall through
          case 118: break;
          case 43:
            { lexemas=yytext(); return T_dato_int;
            }
            // fall through
          case 119: break;
          case 44:
            { lexemas=yytext(); return Auto;
            }
            // fall through
          case 120: break;
          case 45:
            { lexemas=yytext(); return Case;
            }
            // fall through
          case 121: break;
          case 46:
            { lexemas=yytext(); return T_dato_char;
            }
            // fall through
          case 122: break;
          case 47:
            { lexemas=yytext(); return Else;
            }
            // fall through
          case 123: break;
          case 48:
            { lexemas=yytext(); return Enum;
            }
            // fall through
          case 124: break;
          case 49:
            { lexemas=yytext(); return Goto;
            }
            // fall through
          case 125: break;
          case 50:
            { lexemas=yytext(); return T_dato_long;
            }
            // fall through
          case 126: break;
          case 51:
            { lexemas=yytext(); return Main;
            }
            // fall through
          case 127: break;
          case 52:
            { lexemas = yytext(); return Op_booleano_true;
            }
            // fall through
          case 128: break;
          case 53:
            { lexemas=yytext(); return Void;
            }
            // fall through
          case 129: break;
          case 54:
            { lexemas=yytext(); return Break;
            }
            // fall through
          case 130: break;
          case 55:
            { lexemas=yytext(); return Const;
            }
            // fall through
          case 131: break;
          case 56:
            { lexemas = yytext(); return Op_booleano_false;
            }
            // fall through
          case 132: break;
          case 57:
            { lexemas=yytext(); return T_dato_float;
            }
            // fall through
          case 133: break;
          case 58:
            { lexemas=yytext(); return Short;
            }
            // fall through
          case 134: break;
          case 59:
            { lexemas=yytext(); return Union;
            }
            // fall through
          case 135: break;
          case 60:
            { lexemas=yytext(); return While;
            }
            // fall through
          case 136: break;
          case 61:
            { lexemas=yytext(); return T_dato_double;
            }
            // fall through
          case 137: break;
          case 62:
            { lexemas=yytext(); return Extern;
            }
            // fall through
          case 138: break;
          case 63:
            { lexemas=yytext(); return Printf;
            }
            // fall through
          case 139: break;
          case 64:
            { lexemas=yytext(); return Return;
            }
            // fall through
          case 140: break;
          case 65:
            { lexemas=yytext(); return Signed;
            }
            // fall through
          case 141: break;
          case 66:
            { lexemas=yytext(); return Sizeof;
            }
            // fall through
          case 142: break;
          case 67:
            { lexemas=yytext(); return Static;
            }
            // fall through
          case 143: break;
          case 68:
            { lexemas=yytext(); return Cadena;
            }
            // fall through
          case 144: break;
          case 69:
            { lexemas=yytext(); return Struct;
            }
            // fall through
          case 145: break;
          case 70:
            { lexemas=yytext(); return Switch;
            }
            // fall through
          case 146: break;
          case 71:
            { lexemas=yytext(); return Default;
            }
            // fall through
          case 147: break;
          case 72:
            { lexemas=yytext(); return Typedef;
            }
            // fall through
          case 148: break;
          case 73:
            { lexemas=yytext(); return Continue;
            }
            // fall through
          case 149: break;
          case 74:
            { lexemas=yytext(); return Register;
            }
            // fall through
          case 150: break;
          case 75:
            { lexemas=yytext(); return Unsigned;
            }
            // fall through
          case 151: break;
          case 76:
            { lexemas=yytext(); return Volatile;
            }
            // fall through
          case 152: break;
          default:
            zzScanError(ZZ_NO_MATCH);
        }
      }
    }
  }


}
