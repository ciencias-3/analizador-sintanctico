package Analizador;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import java_cup.*;

public class Analizadores {
    public static void main(String[] args) throws Exception {
        String[] pathSintaxis= {"-parser","Sintaxis","src/Analizador/Sintaxis.cup"};
        generarAnalizador(pathSintaxis);        
    }
    public static void generarAnalizador(String[] pathSintaxis) throws IOException, Exception {
        /*Se genera el analizador lexico*/
    	/*Lexico CUP a que hace referencia?*/
    	String[] pathLexico = {"src/Analizador/Lexico.flex","src/Analizador/LexicoCup.flex"};
        jflex.Main.generate(pathLexico);
       
    	/*Se genera el analizador de sintaxis*/
        java_cup.Main.main(pathSintaxis);
        
        /*Para que son estos paths?, si se quitan ni sym ni Sintaxis aparecen*/
        Path pathSymbol = Paths.get("src/Analizador/sym.java");
        if (Files.exists(pathSymbol)) {
            Files.delete(pathSymbol);
        }
        Files.move(
        Paths.get("sym.java"), 
        Paths.get("src/Analizador/sym.java")
        );
        
        Path pathSintaxis1 = Paths.get("src/Analizador/Sintaxis.java");
        if (Files.exists(pathSintaxis1)) {
            Files.delete(pathSintaxis1);
        }
        Files.move(
                Paths.get("Sintaxis.java"), 
                Paths.get("src/Analizador/Sintaxis.java")
        );     
    }
}
