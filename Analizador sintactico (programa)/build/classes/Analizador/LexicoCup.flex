package Analizador;
import java_cup.runtime.Symbol;
%%
%class LexicoCup
%type java_cup.runtime.Symbol
%cup 
%full
%line
%char
L=[a-zA-Z_]+
D=[0-9]+
espacio=[ ,\t,\r]+
%{
    private Symbol symbol(int type, Object value){
        return new Symbol(type, value);
    }
    private Symbol symbol(int type){
        return new Symbol(type);
    }
%}
%%


/* Espacios en blanco */
{espacio} {/*Ignore*/}

/* Salto de linea */
( "\n" ) {return new Symbol(sym.Linea, yytext());}

/* Comillas */
( "\"" ) {return new Symbol(sym.Comillas, yytext());}

/* Tipos de datos */
( int) {return new Symbol(sym.T_dato_int, yytext());}
( char ) {return new Symbol(sym.T_dato_char, yytext());}
( long) {return new Symbol(sym.T_dato_long, yytext());}
( float ) {return new Symbol(sym.T_dato_float, yytext());}
( double ) {return new Symbol(sym.T_dato_double, yytext());}

/* Tipo de dato String */
( string ) {return new Symbol(sym.Cadena, yytext());}

/* Palabra reservada If */
( if ) {return new Symbol(sym.If, yytext());}


/* Palabras reservadas extras */

/* Auto*/
 ( "auto" ) {return new Symbol(sym.Auto, yytext());}
 
/* break*/
 ( "break" ) {return new Symbol(sym.Break, yytext());}
 
 /* case*/
 ( "case" ) {return new Symbol(sym.Case, yytext());}

 
  /* const*/
 ( "const" ) {return new Symbol(sym.Const, yytext());}
 
 /* continue*/
 ( "continue" ) {return new Symbol(sym.Continue, yytext());}
 
  /* default*/
 ( "default" ) {return new Symbol(sym.Default, yytext());}
 
  /* do*/
 ( "do" ) {return new Symbol(sym.Do, yytext());}
 
 /* reservada else*/
 ( "else" ) {return new Symbol(sym.Else, yytext());}
 
 /* enum*/
 ( "enum" ) {return new Symbol(sym.Enum, yytext());}
 
 /* reservada extern*/
 ( "extern" ) {return new Symbol(sym.Extern, yytext());}


/* reservada for*/
 ( "for" ) {return new Symbol(sym.For, yytext());}

/* reservada goto*/
 ( "goto" ) {return new Symbol(sym.Goto, yytext());}

/* reservada register*/
 ( "register" ) {return new Symbol(sym.Register, yytext());}

/* reservada return*/
 ( "return" ) {return new Symbol(sym.Return, yytext());}

/* reservada short*/
 ( "short" ) {return new Symbol(sym.Short, yytext());}

/* reservada signed*/
 ( "signed" ) {return new Symbol(sym.Signed, yytext());}

/* reservada sizeof*/
 ( "sizeof" ) {return new Symbol(sym.Sizeof, yytext());}

/* reservada static*/
 ( "static" ) {return new Symbol(sym.Static, yytext());}

/* reservada struct*/
 ( "struct" ) {return new Symbol(sym.Struct, yytext());}

/* reservada switch*/
 ( "switch" ) {return new Symbol(sym.Switch, yytext());}

/* reservada typedef*/
 ( "typedef" ) {return new Symbol(sym.Typedef, yytext());}

/* reservada union*/
 ( "union" ) {return new Symbol(sym.Union, yytext());}

/* reservada unsigned*/
 ( "unsigned" ) {return new Symbol(sym.Unsigned, yytext());}

/* reservada void*/
 ( "void" ) {return new Symbol(sym.Void, yytext());}

/* reservada volatile*/
 ( "volatile" ) {return new Symbol(sym.Volatile, yytext());}

/* reservada while*/
 ( "while" ) {return new Symbol(sym.While, yytext());}
 
 
 
/* Operador Igual */
( "=" ) {return new Symbol(sym.Igual, yytext());}

/* Operador Suma */
( "+" ) {return new Symbol(sym.Suma, yytext());}

/* Operador Resta */
( "-" ) {return new Symbol(sym.Resta, yytext());}

/* Operador Multiplicacion */
( "*" ) {return new Symbol(sym.Multiplicacion, yytext());}

/* Operador Division */
( "/" ) {return new Symbol(sym.Division, yytext());}

/* Operadores logicos */
( "&&") {return new Symbol(sym.Op_logico_and, yytext());}
( "||") {return new Symbol(sym.Op_logico_or, yytext());}
( "!" ) {return new Symbol(sym.Op_logico_negacion, yytext());}
( "&" ) {return new Symbol(sym.Op_logico_ysolo, yytext());}
( "|" ) {return new Symbol(sym.Op_logico_osolo, yytext());}

/*Operadores Relacionales */
( ">"  ) {lexemas = yytext(), yytext()); return new Symbol(sym.Op_relacional_mayor, yytext());}
( "<"  ) {lexemas = yytext(), yytext()); return new Symbol(sym.Op_relacional_menor, yytext());}
( "==" ) {lexemas = yytext(), yytext()); return new Symbol(sym.Op_relacional_igual, yytext());}
( "!=" ) {lexemas = yytext(), yytext()); return new Symbol(sym.Op_relacional_diferente, yytext());}
( ">=" ) {lexemas = yytext(), yytext()); return new Symbol(sym.Op_relacional_mayor_e_igual, yytext());}
( "<=" ) {lexemas = yytext(), yytext()); return new Symbol(sym.Op_relacional_menor_e_igual, yytext());}
( "<<" ) {lexemas = yytext(), yytext()); return new Symbol(sym.Op_relacional_salida_de_datos, yytext());}
( ">>" ) {lexemas = yytext(), yytext()); return new Symbol(sym.Op_relacional_entrada_de_datos, yytext());}

/* Operadores Atribucion */
( "+=" ) {lexemas = yytext(), yytext()); return new Symbol(sym.Op_atribucion_sumar_a_variable, yytext());}
( "-=" ) {lexemas = yytext(), yytext()); return new Symbol(sym.Op_atribucion_restar_a_variable, yytext());}
( "*=" ) {lexemas = yytext(), yytext()); return new Symbol(sym.Op_atribucion_multiplicar_a_variable, yytext());}
( "/=") {lexemas = yytext(), yytext()); return new Symbol(sym.Op_atribucion_dividir_a_variable, yytext());}
("%=" ) {lexemas = yytext(), yytext()); return new Symbol(sym.Op_atribucion_resto_a_variable, yytext());}

/* Operadores Incremento y decremento */
( "++") {lexemas = yytext(), yytext()); return new Symbol(sym.Op_incremento_aumentar, yytext());}
( "--" ) {lexemas = yytext(), yytext()); return new Symbol(sym.Op_incremento_disminuir, yytext());}

/*Operadores Booleanos*/
(true)      {lexemas = yytext(), yytext()); return new Symbol(sym.Op_booleano_true, yytext());}
(false)      {lexemas = yytext(), yytext()); return new Symbol(sym.Op_booleano_false, yytext());}

/* Parentesis de apertura */
( "(" ) {return new Symbol(sym.Parentesis_a, yytext());}

/* Parentesis de cierre */
( ")" ) {return new Symbol(sym.Parentesis_c, yytext());}

/* Llave de apertura */
( "{" ) {return new Symbol(sym.Llave_a, yytext());}

/* Llave de cierre */
( "}" ) {return new Symbol(sym.Llave_c, yytext());}

/* Corchete de apertura */
( "[" ) {lexemas = yytext(), yytext()); return new Symbol(sym.Corchete_a, yytext());}

/* Corchete de cierre */
( "]" ) {lexemas = yytext(), yytext()); return new Symbol(sym.Corchete_c, yytext());}

/* Marcador de inicio de algoritmo */
( "main" ) {return new Symbol(sym.Main, yytext());}

/* Marcador de inicio de impresion en pantalla */
( "printf" ) {return new Symbol(sym.Printf, yytext());}

/* Punto y coma */
( ", yytext());" ) {return new Symbol(sym.P_coma, yytext());}

/* Punto */
( "." ) {return new Symbol(sym.Punto, yytext());}


/* Identificador */
{L}({L}|{D})* {return new Symbol(sym.Identificador, yytext());}

/* Numero */
("(-"{D}+")")|{D}+ {return new Symbol(sym.Numero, yytext());}



/* Error de analisis */
 . {return new Symbol(sym.ERROR, yytext());}
